# Sample Scala/SBT application

[![build status](https://gitlab.com/paulp/scala-sbt/badges/master/build.svg)](https://gitlab.com/paulp/scala-sbt/commits/master)

A sample application to demonstrate Scala/SBT continuous integration in Gitlab.

## Versions

  - [Scala](http://www.scala-lang.org/) 2.12.1
  - [SBT](http://www.scala-sbt.org/) 0.13.13
  - [Scalatest](http://www.scalatest.org/) 3.0.1
  - [scoverage](https://github.com/scoverage/sbt-scoverage) 1.5.0

## Code
The project content is based on the Activator `minimal-scala` template.

## Prerequisites
You will need to have **Scala** and **sbt** installed to run the project.

## Run tests
Execute `sbt test` in the project root directory.

## Measure test coverage
Execute `sbt clean coverage test coverageReport`.
